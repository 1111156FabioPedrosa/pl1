/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl1;

/**
 *
 * @author fabio
 */
public class Rectangulo {
    private double largura;
    private double altura;

    public Rectangulo(double largura, double altura) {
        setLargura(largura);
        setAltura(altura);
    }

    public double getAltura() {
        return altura;
    }

    public double getLargura() {
        return largura;
    }

    public void setAltura(double altura) {
        if (altura <= 0)
            altura = 1;
        this.altura = altura;
    }

    public void setLargura(double largura) {
        if (largura <= 0)
            largura = 0;
        this.largura = largura;
    }
    
    public double area() {
        return altura * largura;
    }
    
    public double perimetro() {
        return altura * 2 + largura * 2;
    }
}
