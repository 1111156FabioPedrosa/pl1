/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl1;

/**
 *
 * @author fabio
 */
public class TestRectangulo {

    public static void main(String[] args) {
        Rectangulo r1 = new Rectangulo(2, 5);
        System.out.println("Area " + r1.area());
        System.out.println("Perímetro: " + r1.perimetro());
        
        Rectangulo r2 = new Rectangulo(-2, 3);
        System.out.println("Area " + r2.area());
        System.out.println("Perímetro: " + r2.perimetro());
    }
}
