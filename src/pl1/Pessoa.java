/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl1;

/**
 *
 * @author fabio
 */
public class Pessoa {

    private String nome;
    private int idade;

    public Pessoa() {
        this("", 0);
    }

    public Pessoa(String nome) {
        this(nome, 0);
    }

    public Pessoa(String nome, int idade) {
        setNome(nome);
        setIdade(idade);
    }

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setNome(String nome) {
        if (!nome.isEmpty()) {
            this.nome = nome;
        } else {
            this.nome = "sem nome";
        }
    }

    public void setIdade(int idade) {
        if (idade >= 0) {
            this.idade = idade;
        } else {
            this.idade = 0;
        }
    }

    @Override
    public String toString() {
        return this.nome + " tem " + this.idade + " anos.";
    }
}
