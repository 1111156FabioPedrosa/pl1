/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl1;

import java.util.Scanner;

/**
 *
 * @author fabio
 */
public class Ec {

    public static void main(String[] args) {
        //1
        Scanner ler = new Scanner(System.in);
        System.out.println("Como te chamas?");
        String nome = ler.nextLine();
        System.out.println("Olá " + nome + ".");
        
        //2
        Pessoa p1 = new Pessoa("Joana");
        System.out.println("Olá " + p1.getNome());
        p1.setNome("");
        System.out.println("Olá " + p1.getNome());
        p1.setNome("João");
        System.out.println("Olá " + p1.getNome());
        
        //3
        Pessoa p2 = new Pessoa("Catarina", 23);
        System.out.println(p2);
        p2.setIdade(-5);
        System.out.println(p2);
    }
}
